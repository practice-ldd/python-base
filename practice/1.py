#!/usr/bin/python3
# coding:utf-8
import time
import datetime

''' 1.题目：有四个数字：1、2、3、4，能组成多少个互不相同且无重复数字的三位数？各是多少？'''

num = 0
for i in range(1, 5):
    for j in range(1, 5):
        for k in range(1, 5):
            if(i != j) and (i != k) and (j != k):
                num = num+1
                # print(i, j, k)
print("不相同且无重复数字的三位数个数:", num)

''' 2.题目：企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；
利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可提成7.5%；
20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时高于40万元的部分，可提成3%；
60万到100万之间时，高于60万元的部分，可提成1.5%，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放奖金总数？'''

lirun = 450000
jiangjin = 0
print("低于10万部分的奖金：", jiangjin)
if lirun <= 100000:
    jiangjin = lirun * 0.1
if (lirun > 100000) and (lirun <= 200000):
    jiangjin = 10000 + (lirun - 100000) * 0.075
if (lirun > 200000) and (lirun <= 400000):
    jiangjin = 10000 + (200000-100000)*0.705 + (lirun - 200000) * 0.05
if (lirun > 400000) and (lirun <= 600000):
    jiangjin = 10000 + (200000-100000)*0.075 + (400000-200000)*0.05 + (lirun - 400000) * 0.03
if (lirun > 600000) and (lirun <= 100000):
    jiangjin = 10000 + (200000-100000)*0.075 + (400000-200000)*0.05 + (600000 - 400000) * 0.03 + (lirun - 600000) * 0.015
if lirun > 1000000:
    jiangjin = jiangjin + (200000-100000)*0.075 + (400000-200000)*0.05 + (600000 - 400000) * 0.03 + (1000000 - 600000)*0.015 + (lirun - 100000) * 0.01
print(jiangjin)

jiangjin = 0
lirunArr = [1000000, 600000, 400000, 200000, 100000, 0]
rat = [0.01, 0.015, 0.03, 0.05, 0.075, 0.1]
index = 0
for index in range(0, 6):
    if lirun > lirunArr[index]:
        jiangjin += (lirun - lirunArr[index]) * rat[index]
        lirun = lirunArr[index]
print(jiangjin)


'''题目：输入某年某月某日，判断这一天是这一年的第几天？'''
date = "20130320"
isRunnian = int(date[0:4]) % 4 == 0
month = int(date[4:6])
dayth = 0
print(isRunnian)
days = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
for index in range(0, 12):
    dayth = days[index]
    if index+1 == month:
        break
dayth +=  int(date[6:8])
if month > 2 and isRunnian: #闰年大于2月天数+1
    dayth += 1
print(dayth)
print(isRunnian)


'''题目：输入三个整数x,y,z，请把这三个数由小到大输出'''
x = 1
y = 8
z = 15
lst = []
lst.append(x)
lst.append(y)
lst.append(z)
print(lst)
lst.sort(reverse=False)
print(lst)


'''题目：将一个列表的数据复制到另一个列表中。'''
a = [1, 2, 3]
b = a[0:2]
print(b)


'''题目 ： 输出 9*9 乘法口诀表。'''
for i in range(1, 10):
    for j in range(1, 10):
        if i < j:
            continue
        print(i, "*", j, "=", i*j)
    print()


'''题目 ： 暂停一秒输出'''
for i in a:
    print(i)
    #time.sleep(1)#睡眠一秒


'''题目：暂停一秒输出，并格式化当前时间。 '''
print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
# 暂停一秒
time.sleep(0.1)
print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))


'''题目：古典问题：有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问每个月的兔子总数为多少？ '''
months = 22
numList = []
for i in range(1, months):
    if i < 3:
        numList.append(1)
        continue
    else:
        numList.append(numList[i-2] + numList[i-3])
print(numList)


'''题目：打印出所有的"水仙花数"，所谓"水仙花数"是指一个三位数，其各位数字立方和等于该数本身。例如：153是一个"水仙花数"，因为153=1的三次方＋5的三次方＋3的三次方。'''
for n in range(100, 1000):
    i = n // 100
    j = n//10 % 10
    k = n % 10
    if (i**3 + j**3 + k**3 == n):
        print(i, j, k)


'''题目：利用条件运算符的嵌套来完成此题：学习成绩>=90分的同学用A表示，60-89分之间的用B表示，60分以下的用C表示。'''
scoreList = [15, 25, 40, 60, 67, 85, 90, 99, 100]
newList = []
for score in scoreList:
    if score >= 90:
        newList.append("A")
    elif score >= 60 and score < 90:
        newList.append("B")
    else:
        newList.append("C")
print(newList)


'''题目：输出指定格式的日期。'''
# 输出今日日期，格式为 dd/mm/yyyy。更多选项可以查看 strftime() 方法
print(datetime.date.today().strftime('%Y-%m-%d %H:%M:%S'))
# 创建日期对象并格式化
miyazakiBirthDate = datetime.date(1941, 1, 5)
print(miyazakiBirthDate.strftime('%Y-%m-%d %H:%M:%S'))
# 日期算术运算
miyazakiBirthNextDay = miyazakiBirthDate + datetime.timedelta(days=-1)
print(miyazakiBirthNextDay.strftime('%Y-%m-%d %H:%M:%S'))
# 日期替换
miyazakiFirstBirthday = miyazakiBirthDate.replace(year=miyazakiBirthDate.year + 1)
print(miyazakiFirstBirthday.strftime('%Y-%m-%d %H:%M:%S'))


'''题目：一球从100米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在第10次落地时，共经过多少米？第10次反弹多高？'''
high = 100
s = 0
for i in range(10):
    if i == 0:
        s = 100
        high = high/2
    else:
        s += high
        high = high/2
print(s)
print(high)


'''题目：猴子吃桃问题：猴子第一天摘下若干个桃子，当即吃了一半，还不瘾，又多吃了一个第二天早上又将剩下的桃子吃掉一半，又多吃了一个。
以后每天早上都吃了前一天剩下的一半零一个。到第10天早上想再吃时，见只剩下一个桃子了。求第一天共摘了多少。'''
sum = 0
for i in range(10):
    if i == 0:
        sum = 1
    else:
        sum = (sum + 1)*2
print(sum)


'''题目：列表转换为字典。 '''
i = ['a', 'b']
l = [1, 2]
print(dict( [i, l]))