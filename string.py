#!/usr/bin/python3
# coding:utf-8

'''字符串截取'''
var1 = 'Hello World!'
var2 = "Runoob"

print("var1[0]: ", var1[0])
print("var2[2:]",var2[2:])
print("var2[1:5]: ", var2[1:5]) #>=1 <5 ,类似于java subString(1,5)

'''字符串截取和拼接'''
print("new var1 :",var1[:6] + 'Python')

'''Python转义字符:在需要在字符中使用特殊字符时，python用反斜杠(\)转义字符'''
#   \(在行尾时) 	续行符
#   \\ 	反斜杠符号
#   \' 	单引号
#   \" 	双引号
#   \a 	响铃
#   \b 	退格(Backspace)
#   \e 	转义
#   \000 	空
#   \n 	换行
#   \v 	纵向制表符
#   \t 	横向制表符
#   \r 	回车
#   \f 	换页
#   \oyy 	八进制数，yy代表的字符，例如：\o12代表换行
#   \xyy 	十六进制数，yy代表的字符，例如：\x0a代表换行
#   \other 	其它的字符以普通格式输出

'''Python字符串格式化'''
#   %c	 格式化字符及其ASCII码
#   %s	 格式化字符串
#   %d	 格式化整数
#   %u	 格式化无符号整型
#   %o	 格式化无符号八进制数
#   %x	 格式化无符号十六进制数
#   %X	 格式化无符号十六进制数（大写）
#   %f	 格式化浮点数字，可指定小数点后的精度
#   %e	 用科学计数法格式化浮点数
#   %E	 作用同%e，用科学计数法格式化浮点数
#   %g	 %f和%e的简写
#   %G	 %f 和 %E 的简写
#   %p	 用十六进制数格式化变量的地址
name = "小明"
age = 10
print ("我叫 %s 今年 %d 岁!" % (name, age))


'''多行字符串'''
str = """            hello 
            word"""
print(str)


'''字符串内置函数'''
str = "abcdefgabc1_"
''' capitalize() 将字符串的第一个字符转换为大写'''
print(str.capitalize())
''' center(width, fillchar) 返回一个指定的宽度 width 居中的字符串，fillchar 为填充的字符，默认为空格。'''
print(str.center(50,"*"))
''' count(str, beg= 0,end=len(string)) 返回 str 在 string 里面出现的次数，如果 beg 或者 end 指定则返回指定范围内 str 出现的次数'''
print(str.count('a'))
print(str.count('a',0,7))
''' bytes.decode(encoding="utf-8", errors="strict") Python3 中没有 decode 方法，但我们可以使用 bytes 对象的 decode() 方法来解码给定的 bytes 对象，这个 bytes 对象可以由 str.encode() 来编码返回。
#编码和解码'''
print(str.encode("utf-8").decode("utf-8", errors="strict"))
''' endswith(suffix, beg=0, end=len(string)) 以suffer结尾'''
print(str.endswith("c"))
''' find(str, beg=0 end=len(string)) 检测 str 是否包含在字符串中，如果指定范围 beg 和 end ，则检查是否包含在指定范围内，如果包含返回开始的索引值，否则返回-1'''
print(str.find("e"))
print(str.find("m"))
''' isalnum()  如果字符串至少有一个字符并且所有字符都是字母或数字则返 回 True,否则返回 False'''
print(str.isalnum())
''' isspace()  如果字符串中只包含空白，则返回 True，否则返回 False.'''
print(str.isspace())
''' join(seq)  以指定字符串作为分隔符，将 seq 中所有的元素(的字符串表示)合并为一个新的字符串'''
s1 = "-"
s2 = ""
seq = ("r", "u", "n", "o", "o", "b") # 字符串序列
print (s1.join( seq ))
print (s2.join( seq ))





